Program has been tested under Visual Studio 2019

1. Open the folder 'MRProject' and open the solution.
2. Right click on 'MRProject' in the Solution explorer.
3. Under the 'C/C++' click on 'General'.
4. Add the 'include' directory which is in the MRProject folder to the list of 'Additional Include Directories'.
5. Under 'Linker' click on 'General'.
6. Add the 'lib' direcotry which is in the MRProject folder to the list of 'Additional Library Directories'.
7. Press okay

The program should be able to build