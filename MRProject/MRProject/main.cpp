#define _HAS_STD_BYTE 0
#define bins 10
#include <afx.h>
#include <GL/freeglut.h>
#include <iostream>
#include <fstream>
#include <commdlg.h>
#include <assert.h>
#include <filesystem>
#include <glui.h>
#include <filesystem>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <map>
#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include "meshDecimator/mdMeshDecimator.h"
#include <cstdlib>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <FreeImage.h>
#include "annoy/annoylib.h"
#include "annoy/kissrandom.h"

using namespace cv;
using namespace std;
using namespace MeshDecimation;

#define pi 3.14159265359

#pragma region settings
// Uncomment for Building annoy index file
//#define BUILD_ANNOY_INDEX

// uncomment for using annoy rather than brute force
#define ANNOY

// uncomment for generating images for the PSB dataset
//#define CREATE_PSB_IMAGES
#pragma endregion

typedef AnnoyIndex<int, float, Euclidean, Kiss32Random> MyAnnoyIndex;

#pragma region Mesh objects

struct Vertex {
	float x, y, z;
	Vertex operator - (const Vertex& a) const {
		return Vertex{ x - a.x, y - a.y, z - a.z };
	}
	Vertex operator + (const Vertex& a) const {
		return Vertex{ x + a.x, y + a.y, z + a.z };
	}
	Vertex operator * (const float& a) const {
		return Vertex{ x * a, y * a, z * a };
	}
	Vertex operator / (const float& a) const {
		return Vertex{ x / a, y / a, z / a };
	}
	Vertex operator * (const int& a) const {
		return Vertex{ x * a, y * a, z * a };
	}
	Vertex operator / (const int& a) const {
		return Vertex{ x / a, y / a, z / a };
	}
	Vertex cross(const Vertex& a) const {
		return Vertex{ y * a.z - z * a.y, z * a.x - x * a.z, x * a.y - y * a.x };
	}
	float dot(const Vertex& a) const {
		return x * a.x + y * a.y + z * a.z;
	}
	float magnitude() const {
		return sqrtf(x * x + y * y + z * z);
	}
	Vertex normalize() const {
		float m = magnitude();
		return Vertex{ x / m, y / m, z / m };
	}
};

typedef struct Face {
	Face(void) : nverts(0), verts(0) {};
	int nverts;
	Vertex* verts;
	int* indices;										// SWEN The indices to the element in the vertex array
	float normal[3];
} Face;

typedef struct Mesh {
	Mesh(void) : nverts(0), verts(0), nfaces(0), faces(0) {};
	int nverts;
	vector<Vertex> verts;
	//Vertex* verts;
	int nfaces;
	vector<Face> faces;
	//Face* faces;
} Mesh;

#pragma endregion

#pragma region Feature vector object

typedef struct FeatureVector {
public:
	string meshPath;
	string classification;
	// Surface area, compactness, AABB volume, diameter, eccentricity
	float* elemantaryDescriptors;					// Values of the elementary descriptors
	float* standDeviationElementaryDescriptors;		// IF this is the mean feature vector, this variable contains the standard deviation for a variable

	vector<int> angleThreeRandomVerts;
	vector<int> distanceBaryToRandomVert;
	vector<int> distanceTwoRandomVerts;
	vector<int> squareRootAreaTriangle;
	vector<int> cubeRootVolumeOfTetrahedron;

	vector<double> distances;						// The distance for each histogram to the respective mean histogram

	FeatureVector() {
		elemantaryDescriptors = new float[5];
		distances = vector<double>();
	}

	void load(string filePath) {
		string line;					//Where the line is stored after it is read
		ifstream myfile(filePath);		//Opening of the file

		if (myfile.is_open()) {
			// The first line is the mesh path
			getline(myfile, line);
			meshPath = line;

			// The next 5 lines are the elementary descriptors
			for (int i = 0; i < 5; i++) {
				getline(myfile, line);
				elemantaryDescriptors[i] = stof(line);
			}

			// The next 10 (bins) lines are the angles between three random vertices
			for (int i = 0; i < bins; i++) {
				getline(myfile, line);
				angleThreeRandomVerts.push_back(stoi(line));
			}

			// The next 10 (bins) lines are the distances from the bary center to a random vertex
			for (int i = 0; i < bins; i++) {
				getline(myfile, line);
				distanceBaryToRandomVert.push_back(stoi(line));
			}

			// The next 10 (bins) lines are the distances between two random vertices
			for (int i = 0; i < bins; i++) {
				getline(myfile, line);
				distanceTwoRandomVerts.push_back(stoi(line));
			}

			// The next 10 (bins) lines are the area of the square root area of a triangle made up of three random vertices
			for (int i = 0; i < bins; i++) {
				getline(myfile, line);
				squareRootAreaTriangle.push_back(stoi(line));
			}

			// The next 10 (bins) lines are the cube root volume of a tetrahedron
			for (int i = 0; i < bins; i++) {
				getline(myfile, line);
				cubeRootVolumeOfTetrahedron.push_back(stoi(line));
			}

			myfile.close();
		}

	}

	// https://stackoverflow.com/questions/30404099/right-way-to-compute-cosine-similarity-between-two-arrays
	double cosine_similarity(vector<int> A, vector<int> B)
	{
		double dot = 0.0, denom_a = 0.0, denom_b = 0.0;
		for (unsigned int i = 0u; i < A.size(); ++i) {
			dot += A[i] * B[i];
			denom_a += A[i] * A[i];
			denom_b += B[i] * B[i];
		}
		return dot / (sqrt(denom_a) * sqrt(denom_b));
	}

	// Returns the feature vector of the feature vector, but as a float array
	float* getAnnoyFeature() {
		float* sum = new float[10];
		for (int i = 0; i < 5; i++) {
			sum [i] = elemantaryDescriptors[i];
			sum [i + 5] = distances[i];
		}
		return sum;
	}

	// Normalises this vector according to the mean vector
	void normalise(FeatureVector mean) {
		// Firs we calculate how many standard deviations we are away for each of the elementery descriptors
		for (int elem = 0; elem < 5; elem++) {
			elemantaryDescriptors[elem] = (elemantaryDescriptors[elem] - mean.elemantaryDescriptors[elem]) / mean.standDeviationElementaryDescriptors[elem];
		}

		// Then we fill the distances that our histograms have to the mean histogram
		distances.push_back(cosine_similarity(angleThreeRandomVerts, mean.angleThreeRandomVerts));
		distances.push_back(cosine_similarity(distanceBaryToRandomVert, mean.distanceBaryToRandomVert));
		distances.push_back(cosine_similarity(distanceTwoRandomVerts, mean.distanceTwoRandomVerts));
		distances.push_back(cosine_similarity(squareRootAreaTriangle, mean.squareRootAreaTriangle));
		distances.push_back(cosine_similarity(cubeRootVolumeOfTetrahedron, mean.cubeRootVolumeOfTetrahedron));
	}

	void save(string filePath) {
		filePath = "..\\..\\Datasets\\featureVectors\\" + filePath;
		std::ofstream file(filePath);
		if (file.is_open()) {
			file << meshPath << "\n";
			file << elemantaryDescriptors[0] << "\n";
			file << elemantaryDescriptors[1] << "\n";
			file << elemantaryDescriptors[2] << "\n";
			file << elemantaryDescriptors[3] << "\n";
			file << elemantaryDescriptors[4] << "\n";

			for (int i = 0; i < bins; i++)
				file << angleThreeRandomVerts[i] << "\n";

			for (int i = 0; i < bins; i++)
				file << distanceBaryToRandomVert[i] << "\n";

			for (int i = 0; i < bins; i++)
				file << distanceTwoRandomVerts[i] << "\n";

			for (int i = 0; i < bins; i++)
				file << squareRootAreaTriangle[i] << "\n";

			for (int i = 0; i < bins; i++)
				file << cubeRootVolumeOfTetrahedron[i] << "\n";

			file << classification << "\n";
			file.close();
		}
	}
};
#pragma endregion

// Reads the passed file and returns the pointer to a mesh
Mesh* ReadOffFile(const char* filename)
{
	int i;

	// Open file
	FILE* fp;
	if (!(fp = fopen(filename, "r"))) {
		fprintf(stderr, "Unable to open file %s\n", filename);
		return 0;
	}

	// Allocate mesh structure
	Mesh* mesh = new Mesh();
	if (!mesh) {
		fprintf(stderr, "Unable to allocate memory for file %s\n", filename);
		fclose(fp);
		return 0;
	}

	// Read file
	int nverts = 0;
	int nfaces = 0;
	int nedges = 0;
	int line_count = 0;
	char buffer[1024];
	while (fgets(buffer, 1023, fp)) {
		// Increment line counter
		line_count++;

		// Skip white space
		char* bufferp = buffer;
		while (isspace(*bufferp)) bufferp++;

		// Skip blank lines and comments
		if (*bufferp == '#') continue;
		if (*bufferp == '\0') continue;

		// Check section
		if (nverts == 0) {
			// Read header 
			if (!strstr(bufferp, "OFF")) {
				// Read mesh counts
				if ((sscanf(bufferp, "%d%d%d", &nverts, &nfaces, &nedges) != 3) || (nverts == 0)) {
					fprintf(stderr, "Syntax error reading header on line %d in file %s\n", line_count, filename);
					fclose(fp);
					return NULL;
				}

				// Allocate memory for mesh (in the vector we reserve it to prevent underlying array resizes)
				//mesh->verts = new Vertex[nverts];
				//assert(mesh->verts);
				//mesh->faces = new Face[nfaces];
				//assert(mesh->faces);
				mesh->verts.reserve(nverts);
				mesh->faces.reserve(nfaces);
			}
		}
		else if (mesh->nverts < nverts) {
			// Read vertex coordinates
			//Vertex& vert = mesh->verts[mesh->nverts++];
			mesh->verts.push_back(Vertex());
			Vertex& vert = mesh->verts[mesh->nverts];
			mesh->nverts++;
			if (sscanf(bufferp, "%f%f%f", &(vert.x), &(vert.y), &(vert.z)) != 3) {
				fprintf(stderr, "Syntax error with vertex coordinates on line %d in file %s\n", line_count, filename);
				fclose(fp);
				return NULL;
			}
		}
		else if (mesh->nfaces < nfaces) {
			// Get next face
			//Face& face = mesh->faces[mesh->nfaces++];
			mesh->faces.push_back(Face());
			Face& face = mesh->faces[mesh->nfaces];
			mesh->nfaces++;

			// Read number of vertices in face 
			bufferp = strtok(bufferp, " \t");
			if (bufferp) face.nverts = atoi(bufferp);
			else {
				fprintf(stderr, "Syntax error with face on line %d in file %s\n", line_count, filename);
				fclose(fp);
				return NULL;
			}

			// Allocate memory for face vertices and the indices
			face.verts = new Vertex[face.nverts];
			face.indices = new int[face.nverts];
			assert(face.verts);
			assert(face.indices);

			// Read vertex indices for face
			for (i = 0; i < face.nverts; i++) {
				bufferp = strtok(NULL, " \t");
				if (bufferp) {
					face.verts[i] = mesh->verts[atoi(bufferp)];
					face.indices[i] = atoi(bufferp);					// SWEN we also save the index to the vertex
				}
				else {
					fprintf(stderr, "Syntax error with face on line %d in file %s\n", line_count, filename);
					fclose(fp);
					return NULL;
				}
			}

			// Compute normal for face
			face.normal[0] = face.normal[1] = face.normal[2] = 0;
			Vertex v1 = face.verts[face.nverts - 1];
			for (i = 0; i < face.nverts; i++) {
				Vertex v2 = face.verts[i];
				face.normal[0] += (v1.y - v2.y) * (v1.z + v2.z);
				face.normal[1] += (v1.z - v2.z) * (v1.x + v2.x);
				face.normal[2] += (v1.x - v2.x) * (v1.y + v2.y);
				v1 = v2;
			}

			// Normalize normal for face
			float squared_normal_length = 0.0;
			squared_normal_length += face.normal[0] * face.normal[0];
			squared_normal_length += face.normal[1] * face.normal[1];
			squared_normal_length += face.normal[2] * face.normal[2];
			float normal_length = sqrt(squared_normal_length);
			if (normal_length > 1.0E-6) {
				face.normal[0] /= normal_length;
				face.normal[1] /= normal_length;
				face.normal[2] /= normal_length;
			}
		}
		else {
			// Should never get here
			fprintf(stderr, "Found extra text starting at line %d in file %s\n", line_count, filename);
			break;
		}
	}

	// Check whether read all faces
	if (nfaces != mesh->nfaces) {
		fprintf(stderr, "Expected %d faces, but read only %d faces in file %s\n", nfaces, mesh->nfaces, filename);
	}

	// Close file
	fclose(fp);

	// Return mesh 
	return mesh;
}

void CallBack(const char* msg)
{
	cout << msg;
}

#pragma region Shape class with feature descriptor functions

// Shape definition
typedef struct Shape {
	Shape(string meshPath, string classification, string filename) {
		this->meshPath = meshPath;
		this->classification = classification;
		this->filename = filename;
	};

	std::string classification;			//The classification for this mesh
	std::string meshPath;				//The path to the mesh file (so we can load the mesh file only when needed)
	Mesh* mesh;							//Where the mesh will be stored
	string filename;					//Name of the file that will be used when saving the feature vector
	Eigen::Matrix3f eigenVectors;		//Matrix in which the eigenvectors are stored
	vector<float> aabb;					//The AABB of this mesh
	const int histogramRuns = 50000;	//The number of samples that is taken for each histogram

	void loadMesh() {
		// Initialize the AABB (with NULL)
		aabb.push_back(NULL); aabb.push_back(NULL); aabb.push_back(NULL);
		aabb.push_back(NULL); aabb.push_back(NULL); aabb.push_back(NULL);

		this->mesh = ReadOffFile(this->meshPath.c_str());
	}

	FeatureVector getAllFeatures() {
		onlyTriangles();
		
		if (mesh->nverts > 5475) {
			simplify(5261);
		}
		if (mesh->nverts < 5047) {
			unSimplify(5261);
		}

		// Get the features
		poseNormalization();
		float surfaceArea = getSurfaceArea();
		float compactness = getCompactness();
		float aabbVolume = getAABBVolume();
		float diameter = getDiameter();
		float eccentricity = getEccentricity();

		vector<int> A3 = getHistogramA3();
		vector<int> D1 = getHistogramD1();
		vector<int> D2 = getHistogramD2();
		vector<int> D3 = getHistogramD3();
		vector<int> D4 = getHistogramD4();

		// Make the feature vector
		FeatureVector f = FeatureVector();
		f.meshPath = meshPath;
		f.classification = classification;

		f.elemantaryDescriptors[0] = surfaceArea;
		f.elemantaryDescriptors[1] = compactness;
		f.elemantaryDescriptors[2] = aabbVolume;
		f.elemantaryDescriptors[3] = diameter;
		f.elemantaryDescriptors[4] = eccentricity;

		f.angleThreeRandomVerts = A3;
		f.distanceBaryToRandomVert = D1;
		f.distanceTwoRandomVerts = D2;
		f.squareRootAreaTriangle = D3;
		f.cubeRootVolumeOfTetrahedron = D4;

		f.save(filename);
		return f;
	}

	void onlyTriangles() {
		for (int i = 0; i < mesh->nfaces; i++) {
			if (mesh->faces[i].nverts != 3) {
				cout << "Mesh " << this->meshPath << "has other than 3 verts: " << mesh->faces[i].nverts << std::endl;
			}
		}
	}

	// Simplifies the mesh to the target number of vertices
	void simplify(int vertices) {
		// The decimator wants a vec3 with floats/ints for the vertices/points
		Vec3<float>* points = new Vec3<float>[mesh->nverts];
		for (int i = 0; i < this->mesh->nverts; i++) {
			Vertex& v = this->mesh->verts[i];
			points[i] = Vec3<float>(v.x, v.y, v.z);
		}

		Vec3<int>* triangles = new Vec3<int>[mesh->nfaces];
		for (int i = 0; i < this->mesh->nfaces; i++) {
			Face& v = this->mesh->faces[i];
			if (v.nverts > 3) {
				cout << "ERROR face has more than 3 vertices" << std::endl;
			}
			triangles[i] = Vec3<int>(v.indices[0], v.indices[1], v.indices[2]);
		}

		// decimate mesh
		MeshDecimator myMDecimator;
		myMDecimator.SetCallBack(&CallBack);
		myMDecimator.Initialize(mesh->nverts, mesh->nfaces, &points[0], &triangles[0]);
		//myMDecimator.Decimate(targetNVerticesDecimatedMesh, targetNTrianglesDecimatedMesh, maxDecimationError);
		myMDecimator.Decimate(vertices, 0, 1.0f);

		// allocate memory for decimated mesh
		vector< Vec3<Float> > decimatedPoints;
		vector< Vec3<int> > decimatedtriangles;
		decimatedPoints.resize(myMDecimator.GetNVertices());
		decimatedtriangles.resize(myMDecimator.GetNTriangles());

		// retreive decimated mesh
		myMDecimator.GetMeshData(&decimatedPoints[0], &decimatedtriangles[0]);

		// We set the correct number of vertices/triangles
		mesh->nverts = decimatedPoints.size();
		mesh->nfaces = decimatedtriangles.size();

		// We convert the vec3 with float/int back to the vertices/points
		mesh->verts.clear();
		for (int i = 0; i < decimatedPoints.size(); i++) {
			Vertex v = Vertex();
			v.x = decimatedPoints[i][0];
			v.y = decimatedPoints[i][1];
			v.z = decimatedPoints[i][2];

			mesh->verts.push_back(v);
		}

		mesh->faces.clear();
		for (int i = 0; i < decimatedtriangles.size(); i++) {
			Face f = Face();
			f.nverts = 3;
			f.verts = new Vertex[f.nverts];
			f.indices = new int[f.nverts];

			for (int j = 0; j < 3; j++) {
				f.indices[j] = decimatedtriangles[i][j];
				f.verts[j] = mesh->verts[f.indices[j]];
			}
			mesh->faces.push_back(f);
		}
	}

	// https://graphics.stanford.edu/~mdfisher/subdivision.html
	void unSimplify(int vertices) {
		// We determine the difference between the number of vertices we have and the amound we want and divide that by 3
		int numberToMake = (vertices - mesh->nverts) / 3;

		// We pick a random face and subdivide that
		for (int i = 0; i < numberToMake; i++) {
			int random = rand() % mesh->nfaces;
			// We get the existing vertices
			// A------B
			// \     /
			//  \   /
			//    C
			Vertex A = mesh->faces[random].verts[0];
			int AIndex = mesh->faces[random].indices[0];

			Vertex B = mesh->faces[random].verts[1];
			int BIndex = mesh->faces[random].indices[1];

			Vertex C = mesh->faces[random].verts[2];
			int CIndex = mesh->faces[random].indices[2];

			// Generate the new vertices and faces (and add them to the mesh)
			// A------AB------B
			// \  1  /  \  3 /
			//  \   /  2 \  /
			//   CA ------ BC
			//    \   4   /
			//     \     /
			//        C
			Vertex AB = (A + B) / 2;
			int ABIndex = mesh->nverts;
			mesh->verts.push_back(AB); mesh->nverts++;

			Vertex BC = (B + C) / 2;
			int BCIndex = mesh->nverts;
			mesh->verts.push_back(BC); mesh->nverts++;

			Vertex CA = (C + A) / 2;
			int CAIndex = mesh->nverts;
			mesh->verts.push_back(CA); mesh->nverts++;

			// The 'original' face will be face 1
			mesh->faces[random].verts[1] = AB; mesh->faces[random].indices[1] = ABIndex;
			mesh->faces[random].verts[2] = CA; mesh->faces[random].indices[2] = CAIndex;

			// Face 2
			Face f2 = Face();
			f2.nverts = 3; f2.verts = new Vertex[3]; f2.indices = new int[3];
			f2.verts[0] = AB; f2.indices[0] = ABIndex;
			f2.verts[1] = BC; f2.indices[1] = BCIndex;
			f2.verts[2] = CA; f2.indices[2] = CAIndex;
			mesh->faces.push_back(f2);
			mesh->nfaces++;

			// Face 3
			Face f3 = Face();
			f3.nverts = 3; f3.verts = new Vertex[3]; f3.indices = new int[3];
			f3.verts[0] = AB; f3.indices[0] = ABIndex;
			f3.verts[1] = B;  f3.indices[1] = BIndex;
			f3.verts[2] = BC; f3.indices[2] = BCIndex;
			mesh->faces.push_back(f3);
			mesh->nfaces++;

			// Face 4
			Face f4 = Face();
			f4.nverts = 3; f4.verts = new Vertex[3]; f4.indices = new int[3];
			f4.verts[0] = CA; f4.indices[0] = CAIndex;
			f4.verts[1] = BC; f4.indices[1] = BCIndex;
			f4.verts[2] = C;  f4.indices[2] = CIndex;
			mesh->faces.push_back(f4);
			mesh->nfaces++;
		}

		int x = 0;
	}

	// Features
	// Updates the list of vertices in a mesh by taking the value from a face and placing that at the corresponding index
	void updateVertices() {
		for (int f = 0; f < mesh->nfaces; f++)
			for (int i = 0; i < 3; i++)
				mesh->verts[mesh->faces[f].indices[i]] = mesh->faces[f].verts[i];
	}

	Vertex getCentroid() {
		// Calculate the centroid
		float surfaceArea = 0.0f;
		Vertex centroid{ 0.0f, 0.0f, 0.0f };
		for (int f = 0; f < mesh->nfaces; f++)
		{
			Vertex center = (mesh->faces[f].verts[0] + mesh->faces[f].verts[1] + mesh->faces[f].verts[2]) / 3.0f;
			float area = 0.5f * ((mesh->faces[f].verts[1] - mesh->faces[f].verts[0]).cross(mesh->faces[f].verts[2] - mesh->faces[f].verts[0])).magnitude();
			centroid = centroid + (center * area);
			surfaceArea += area;
		}
		return (centroid / surfaceArea);
	}

	float getSignedTriangleVolume(Vertex v1, Vertex v2, Vertex v3) {
		// Calculate the volume of one tetrahedron
		float v321 = v3.x * v2.y * v1.z;
		float v231 = v2.x * v3.y * v1.z;
		float v312 = v3.x * v1.y * v2.z;
		float v132 = v1.x * v3.y * v2.z;
		float v213 = v2.x * v1.y * v3.z;
		float v123 = v1.x * v2.y * v3.z;
		return (1.0f / 6.0f) * (-v321 + v231 + v312 - v132 - v213 + v123);
	}

	float getVolume() {
		// Calculate the sum of all the tetrahedrons
		float volumes = 0;
		for (int f = 0; f < mesh->nfaces; f++)
			volumes += getSignedTriangleVolume(mesh->faces[f].verts[0], mesh->faces[f].verts[1], mesh->faces[f].verts[2]);
		float totalVolume = abs(volumes);
		std::cout << "volume: " << totalVolume << endl;
		return totalVolume;
	}

	// Determines the whether more volume of the mesh is located on the positive
	// or the negative side of the plane
	//
	// p == 0 -> x = 0 plane
	// p == 1 -> y = 0 plane
	// p == 2 -> z = 0 plane
	bool volumeSymmetry(int p) {
		float volumes = 0;
		vector<Vertex> verts(3);
		for (int f = 0; f < mesh->nfaces; f++)
		{
			// Do not calculate the volume of this triangle if it is on the negative side of the plane
			if (p == 0 && mesh->faces[f].verts[0].x < 0 && mesh->faces[f].verts[1].x < 0 && mesh->faces[f].verts[2].x < 0)	continue;
			else if (p == 1 && mesh->faces[f].verts[0].y < 0 && mesh->faces[f].verts[1].y < 0 && mesh->faces[f].verts[2].y < 0)continue;
			else if (p == 2 && mesh->faces[f].verts[0].z < 0 && mesh->faces[f].verts[1].z < 0 && mesh->faces[f].verts[2].z < 0)	continue;
			// If one of the triangle's vertices is on the negative side of the plane make it zero
			// This is an approximation as a tiny volume will be lost
			for (int i = 0; i < 3; i++)
			{
				verts[i] = mesh->faces[f].verts[i];
				switch (p) {
				case 0:
					if (verts[i].x < 0)
						verts[i].x = 0;
					break;
				case 1:
					if (verts[i].y < 0)
						verts[i].y = 0;
					break;
				case 2:
					if (verts[i].z < 0)
						verts[i].z = 0;
					break;
				default:
					std::cout << "we are not supposed to get here" << endl;
				}

			}
			volumes += getSignedTriangleVolume(verts[0], verts[1], verts[2]);
		}
		float halfVolume = abs(volumes);
		std::cout << "half volume: " << halfVolume << endl;
		float totalVolume = getVolume();
		return halfVolume > (totalVolume - halfVolume);
	}

	void poseNormalization() {
		float minX, maxX, minY, maxY, minZ, maxZ;
		minX = minY = minZ = 1e38f;
		maxX = maxY = maxZ = -1e38f;
		// Get the centroid
		Vertex centroid = getCentroid();
		// Translate the mesh so that the centroid is as close to the world origin
		for (int f = 0; f < mesh->nfaces; f++)
			for (int i = 0; i < 3; i++)
			{
				mesh->faces[f].verts[i] = mesh->faces[f].verts[i] - centroid;
				// Find the min and max values
				if (mesh->faces[f].verts[i].x < minX)	minX = mesh->faces[f].verts[i].x;
				if (mesh->faces[f].verts[i].y < minY)	minY = mesh->faces[f].verts[i].y;
				if (mesh->faces[f].verts[i].z < minZ)	minZ = mesh->faces[f].verts[i].z;
				if (mesh->faces[f].verts[i].x > maxX)	maxX = mesh->faces[f].verts[i].x;
				if (mesh->faces[f].verts[i].y > maxY)	maxY = mesh->faces[f].verts[i].y;
				if (mesh->faces[f].verts[i].z > maxZ)	maxZ = mesh->faces[f].verts[i].z;
			}

		// Rotate the mesh via eigen vectors
		Eigen::MatrixXf points(mesh->nfaces * 3, 3);
		Eigen::Matrix3f eigens;
		Eigen::MatrixXf cov;
		eigens.setZero();
		// Repeat until convergence
		while (!(abs(eigens(0, 0)) == abs(eigens(1, 1)) == abs(eigens(2, 2)) == 1))
		{
			int c = 0;
			// fill the matrix
			for (int f = 0; f < mesh->nfaces; f++)
				for (int i = 0; i < 3; i++)
				{
					Eigen::Vector3f v(mesh->faces[f].verts[i].x, mesh->faces[f].verts[i].y, mesh->faces[f].verts[i].z);
					points.row(c) = v;
					c++;
				}
			// center the matrix
			Eigen::MatrixXf centered = points.rowwise() - points.colwise().mean();
			cov = centered.adjoint() * centered;
			Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf> eig(cov);
			// Retrieve the eigen vectors
			eigens = eig.eigenvectors();
			Eigen::MatrixXf newVertices = eig.eigenvectors().inverse().transpose() * points.transpose();
			newVertices.transposeInPlace();
			// Transform the vertices
			int c2 = 0;
			for (int f = 0; f < mesh->nfaces; f++)
				for (int i = 0; i < 3; i++)
				{
					Eigen::Vector3f v = newVertices.row(c2);
					mesh->faces[f].verts[i].x = newVertices(c2, 0);
					mesh->faces[f].verts[i].y = newVertices(c2, 1);
					mesh->faces[f].verts[i].z = newVertices(c2, 2);
					c2++;
				}
		}
		eigenVectors = cov;
		std::cout << "eigens: " << eigens << endl;

		// Scale mesh to fit inside unit cube (-0.5 to 0.5 in all dimensions)
		float scaleFactor = 1.0f / max(abs(maxX - minX), max(abs(maxY - minY), abs(maxZ - minZ)));
		for (int f = 0; f < mesh->nfaces; f++)
			for (int i = 0; i < 3; i++)
				mesh->faces[f].verts[i] = mesh->faces[f].verts[i] * scaleFactor;
		// Flip the mesh
		momentFlip();
		// update the list of vertices
		updateVertices();
		// After pose normalization aabb is no longer correct
		aabb = vector<float>(6, NULL);
	}

	// Mirrors the mesh based on the moment
	void momentFlip() {
		for (int i = 0; i < 3; i++)
		{
			if (!volumeSymmetry(i))
			{
				switch (i) {
				case 0:
					for (int f = 0; f < mesh->nfaces; f++)
						for (int i = 0; i < 3; i++)
							mesh->faces[f].verts[i].x *= -1;
					break;
				case 1:
					for (int f = 0; f < mesh->nfaces; f++)
						for (int i = 0; i < 3; i++)
							mesh->faces[f].verts[i].y *= -1;
					break;
				case 2:
					for (int f = 0; f < mesh->nfaces; f++)
						for (int i = 0; i < 3; i++)
							mesh->faces[f].verts[i].z *= -1;
					break;
				default:
					std::cout << "we are not supposed to get here" << endl;
				}
				std::cout << "flipped: " << i << endl;
			}
		}
	}

	float getSurfaceArea() {
		float surfaceArea = 0.0f;
		for (int f = 0; f < mesh->nfaces; f++)
		{
			// get surface area of a face (triangle) and add it to the total
			Vertex center = (mesh->faces[f].verts[0] + mesh->faces[f].verts[1] + mesh->faces[f].verts[2]) / 3;
			float area = ((mesh->faces[f].verts[1] - mesh->faces[f].verts[0]).cross(mesh->faces[f].verts[2] - mesh->faces[f].verts[0])).magnitude();
			surfaceArea += area;
		}
		surfaceArea *= 0.5f;
		std::cout << "surface area: " << surfaceArea << endl;
		return surfaceArea;
	}

	float getCompactness() {
		float surfaceArea = getSurfaceArea();
		float volume = getVolume();
		float compactness = (surfaceArea / volume) * sqrtf(surfaceArea / (4.0f * pi)) / 3.0f;
		std::cout << "compactness: " << compactness << endl;
		return compactness;
	}

	// needs pose normalization for the eigenvectors
	float getEccentricity() {
		float one = Vertex{ eigenVectors(0, 0), eigenVectors(0, 1), eigenVectors(0, 2) }.magnitude();
		float two = Vertex{ eigenVectors(1, 0), eigenVectors(1, 1), eigenVectors(1, 2) }.magnitude();
		float three = Vertex{ eigenVectors(2, 0), eigenVectors(2, 1), eigenVectors(2, 2) }.magnitude();
		std::cout << "eigenvector lengths: " << one << ", " << two << ", " << three << endl;
		// Not really sure about this, what about the third eigenvector?
		float eccentricity = one / two;
	std:cout << "eccentricity: " << eccentricity << endl;
		return eccentricity;
	}

	float getDiameter() {
		float diameter = 0.0f;
		for (int i = 0; i < mesh->nverts; i++)
			for (int j = 0; j < mesh->nverts; j++)
			{
				float distance = sqrtf(pow((mesh->verts[j].x - mesh->verts[i].x), 2) +
					pow((mesh->verts[j].y - mesh->verts[i].y), 2) +
					pow((mesh->verts[j].z - mesh->verts[i].z), 2));
				if (diameter < distance)
					diameter = distance;
			}
		std::cout << "diameter: " << diameter << endl;
		return diameter;
	}

	// Return min max aabb values in the form of [minx, miny, minz, maxx, maxy, maxz]
	vector<float> getAABB() {
		for (int f = 0; f < mesh->nfaces; f++)
			for (int i = 0; i < 3; i++)
			{
				if (mesh->faces[f].verts[i].x < aabb[0]) aabb[0] = mesh->faces[f].verts[i].x;
				if (mesh->faces[f].verts[i].y < aabb[1]) aabb[1] = mesh->faces[f].verts[i].y;
				if (mesh->faces[f].verts[i].z < aabb[2]) aabb[2] = mesh->faces[f].verts[i].z;
				if (mesh->faces[f].verts[i].x > aabb[3]) aabb[3] = mesh->faces[f].verts[i].x;
				if (mesh->faces[f].verts[i].y > aabb[4]) aabb[4] = mesh->faces[f].verts[i].y;
				if (mesh->faces[f].verts[i].z > aabb[5]) aabb[5] = mesh->faces[f].verts[i].z;
			}
		return aabb;
	}

	float getAABBVolume() {
		aabb = getAABB();
		float volume = abs(aabb[3] - aabb[0]) * abs(aabb[3] - aabb[0]) * abs(aabb[3] - aabb[0]);
		std::cout << "bounding box volume: " << volume << endl;
		std::cout << "min x: " << aabb[0] << ", max x: " << aabb[3] << endl;
		std::cout << "min y: " << aabb[1] << ", max y: " << aabb[4] << endl;
		std::cout << "min z: " << aabb[2] << ", max z: " << aabb[5] << endl;
		return volume;
	}

	// Determines the histogram of angles between 3 random points
	vector<int> getHistogramA3() {
		// Histogram has 10 bins with angles ranging in total from 0 to 180
		vector<int> histogram = vector<int>();
		for (int i = 0; i < bins; i++)
			histogram.push_back(0);

		const float step = 180 / bins;
		// Array of random vertices
		int v[3];
		for (int i = 0; i < histogramRuns; i++)
		{
			for (int j = 0; j < 3; j++)
				v[j] = rand() % mesh->nverts;
			Vertex ba = (mesh->verts[v[0]] - mesh->verts[v[1]]).normalize();
			Vertex bc = (mesh->verts[v[2]] - mesh->verts[v[1]]).normalize();
			// Calculate angle in degrees
			float angle = acos(ba.dot(bc));
			angle *= 180 / pi;

			// Put into histogram bins
			for (int j = 0; j < bins; j++)
			{
				if (angle > j* step)
					if (angle <= (j + 1) * step)
						histogram[j]++;
			}
		}
		// Output created histogram
		std::cout << "Histogram of angles between 3 random points: " << endl << "[" << histogram[0];
		for (int i = 1; i < bins; i++)
			std::cout << ", " << histogram[i];
		std::cout << "]" << endl;
		return histogram;
	}

	// Determines the histogram of the distance from the centroid to a random vertex
	vector<int> getHistogramD1() {
		// Histogram has 10 bins with distance ranging in total from 0 to root of two divided by two
		vector<int> histogram = vector<int>();
		for (int i = 0; i < bins; i++)
			histogram.push_back(0);

		const float step = 0.5f * sqrtf(2.0f) / bins;
		for (int i = 0; i < histogramRuns; i++)
		{
			// Index of random vertex
			int v = rand() % mesh->nverts;
			// Since the centroid is now at the origin we can just take the magnitude
			float distance = mesh->verts[v].magnitude();
			// Put into histogram bins
			for (int j = 0; j < bins; j++)
			{
				if (distance > j* step)
					if (distance <= (j + 1) * step)
						histogram[j]++;
			}
		}
		// Output created histogram
		std::cout << "Histogram of distance to the centroid: " << endl << "[" << histogram[0];
		for (int i = 1; i < bins; i++)
			std::cout << ", " << histogram[i];
		std::cout << "]" << endl;
		return histogram;
	}

	// Determines the histogram of the distance between two random vertices
	vector<int> getHistogramD2() {
		// Histogram has 10 bins with distance ranging in total from 0 to root of two
		vector<int> histogram = vector<int>();
		for (int i = 0; i < bins; i++)
			histogram.push_back(0);
		const float step = sqrtf(2.0f) / bins;
		for (int i = 0; i < histogramRuns; i++)
		{
			// Random vertices
			Vertex one = mesh->verts[rand() % mesh->nverts];
			Vertex two = mesh->verts[rand() % mesh->nverts];
			float distance = sqrtf(pow(two.x - one.x, 2) + pow(two.y - one.y, 2) + pow(two.z - one.z, 2));
			// Put into histogram bins
			for (int j = 0; j < bins; j++)
			{
				if (distance > j* step)
					if (distance <= (j + 1) * step)
						histogram[j]++;
			}
		}
		// Output created histogram
		std::cout << "Histogram of distance between two random points: " << endl << "[" << histogram[0];
		for (int i = 1; i < bins; i++)
			std::cout << ", " << histogram[i];
		std::cout << "]" << endl;
		return histogram;
	}

	// Determines the histogram of the square root of a triangle made up of 3 random vertices
	vector<int> getHistogramD3() {
		// Histogram has 10 bins with distance ranging in total from 0 to 1
		vector<int> histogram = vector<int>();
		for (int i = 0; i < bins; i++)
			histogram.push_back(0);

		const float step = 1.0f / bins;
		for (int i = 0; i < histogramRuns; i++)
		{
			// Random vertices
			Vertex one = mesh->verts[rand() % mesh->nverts];
			Vertex two = mesh->verts[rand() % mesh->nverts];
			Vertex three = mesh->verts[rand() % mesh->nverts];
			float area = sqrtf(0.5f * ((two - one).cross(three - one)).magnitude());
			// Put into histogram bins
			for (int j = 0; j < bins; j++)
			{
				if (area > j* step)
					if (area <= (j + 1) * step)
						histogram[j]++;
			}
		}
		// Output created histogram
		std::cout << "Histogram of square root of a triangle made up of 3 random points: " << endl << "[" << histogram[0];
		for (int i = 1; i < bins; i++)
			std::cout << ", " << histogram[i];
		std::cout << "]" << endl;
		return histogram;
	}

	// Determines the histogram of the cube root of volume of tetrahedron formed by 4 random vertices
	vector<int> getHistogramD4() {
		// Histogram has 10 bins with distance ranging in total from 0 to roughly 0.7
		vector<int> histogram = vector<int>();
		for (int i = 0; i < bins; i++)
			histogram.push_back(0);

		const float step = 0.7f / bins;
		for (int i = 0; i < histogramRuns; i++)
		{
			// Random vertices
			Vertex one = mesh->verts[rand() % mesh->nverts];
			Vertex two = mesh->verts[rand() % mesh->nverts];
			Vertex three = mesh->verts[rand() % mesh->nverts];
			Vertex four = mesh->verts[rand() % mesh->nverts];
			float volume = abs((one - four).dot((two - four).cross(three - four))) / 6.0f;
			volume = cbrtf(volume);
			// Put into histogram bins
			for (int j = 0; j < bins; j++)
			{
				if (volume > j* step)
					if (volume <= (j + 1) * step)
						histogram[j]++;
			}
		}
		// Output created histogram
		std::cout << "Histogram of the cube root of volume of tetrahedron formed by 4 random vertices: " << endl << "[" << histogram[0];
		for (int i = 1; i < bins; i++)
			std::cout << ", " << histogram[i];
		std::cout << "]" << endl;
		return histogram;
	}
};

#pragma endregion

#pragma region Definitions

////////////////////////////////////////////////////////////
// GLUT variables 
////////////////////////////////////////////////////////////
static int GLUTwindow = 0;
static int GLUTwindow_height = 800;
static int GLUTwindow_width = 800;
static int GLUTmouse[2] = { 0, 0 };
static int GLUTbutton[3] = { 0, 0, 0 };
static int GLUTarrows[4] = { 0, 0, 0, 0 };
static int GLUTmodifiers = 0;

////////////////////////////////////////////////////////////
// Display variables
////////////////////////////////////////////////////////////
static int scaling = 0;
static int translating = 0;
static int rotating = 0;
static float scale = 1.0;
static float center[3] = { 0.0, 0.0, 0.0 };
static float rotation[3] = { 0.0, 0.0, 0.0 };
static float translation[3] = { 0.0, 0.0, -4.0 };

////////////////////////////////////////////////////////////
// Header file replacement/method definitions
////////////////////////////////////////////////////////////
Shape* s = NULL;
vector<Shape> shapes;										// All the shapes that we want to show to our users
vector<FeatureVector> featureVectors;						// All the feature vectors that we have
FeatureVector meanFeatureVector;							// THIS is the mean feature vector as computed after running -> normalizeFeatureVectors()
MyAnnoyIndex* theIndex = NULL;								// The variable that contains the tree for ANOY
void queryMesh();											// Dialogue to open a specific file
vector<Shape> getFilesInFolder();							// List all the paths to the off files
void loadFeatureVectors();									// Loads all the feature files from the featureVectors
void normalizeFeatureVectors();								// Normalizes all the feature vectors
void determineStandardDeviation(vector<Shape>);				// Determines the standard deviation of the list of shapes
void runEvaluation();										// Runs the evaluation on the dataset

void showResults();
void generateImage();
vector<Shape> imageShapes = vector<Shape>();
int imageIndex = -1;
void getImage();
char workingDirectory[_MAX_PATH];
vector<tuple<float, FeatureVector>> query;
vector<float> neighbor_dist;


// Glut
void GLUTInit(int* argc, char** argv);
void GLUTMainLoop(void);
void GLUTResize(int w, int h);
void GLUTMouse(int button, int state, int x, int y);
void GLUTKeyboard(unsigned char key, int x, int y);
void GLUTSpecial(int key, int x, int y);
void GLUISetup();

// GLUI
int wireframe = false;

#pragma endregion

// Program entry point
int main(int argc, char** argv) {
	_fullpath(workingDirectory, ".\\", _MAX_PATH);
	// init rand
	srand(time(NULL));

	//shapes = getFilesInFolder();			// Scans the folder and get all the shapes
	loadFeatureVectors();					// Loads all the feature vectors from the feature vectors folder	
	//runEvaluation();
	//determineStandardDeviation(shapes);	// Determines the standard deviation of the number of vertices for all shapes in the "shapes" list

	// Initialize GLUT
	GLUTInit(&argc, argv);
	GLUISetup();
	GLUTMainLoop();
}

#pragma region GLUI
void GLUISetup() {
	// General stuff
	GLUI_Master.set_glutKeyboardFunc(GLUTKeyboard);
	GLUI_Master.set_glutSpecialFunc(GLUTSpecial);
	GLUI_Master.set_glutMouseFunc(GLUTMouse);
	GLUI_Master.set_glutReshapeFunc(GLUTResize);
	GLUI_Master.set_glutIdleFunc(NULL);
	// Create window
	GLUI* glui = GLUI_Master.create_glui_subwindow(GLUTwindow, GLUI_SUBWINDOW_LEFT);
	// Elements
	new GLUI_Button(glui, "Load file", 0, (GLUI_Update_CB)queryMesh);
	GLUI_Panel* optionPanel = glui->add_panel("Mesh options");
	new GLUI_Checkbox(optionPanel, "Wireframe", &wireframe);
	new GLUI_Button(glui, "query", 0, (GLUI_Update_CB)showResults);
#ifdef CREATE_PSB_IMAGES
	new GLUI_Button(glui, "start image generation", 0, (GLUI_Update_CB)generateImage);
	new GLUI_Button(glui, "generate image", 0, (GLUI_Update_CB)getImage);
#endif
	glui->set_main_gfx_window(GLUTwindow);
}
#pragma endregion

#pragma region Image generation

#ifdef CREATE_PSB_IMAGES
	// Load all off files and generate the images
	void generateImage() {
		std::string path = "../../Datasets";
	
		// First get the paths for the PSB dataset
		for (const auto& subFolder : std::filesystem::directory_iterator(path + "/PSB")) {
			// There are folders within the path which we will have to open, we will iterate over those (these folders are the classifications)
			std::filesystem::path classificationFolder = subFolder.path();
	
			// We iterate over the files within the folder
			for (const auto& entry : std::filesystem::directory_iterator(classificationFolder.string())) {
				std::filesystem::path fileEntry = entry.path();
	
				// In these folders there are both .off and .txt files, we only want the off file (we add those to the path list)
				if (fileEntry.extension().string() == ".off") {
	
					// Split the line in parts (based on the '\\') to get the filename xxx.off, which will be used to save the feature vector
					std::stringstream ss(fileEntry.string());
					std::string token;
					vector<string> parts = vector<string>();
					while (std::getline(ss, token, '\\')) {
						parts.push_back(token);
					}
					imageShapes.push_back(Shape(fileEntry.string(), classificationFolder.filename().string(), parts[parts.size() - 1]));
				}
			}
		}
		imageIndex = 0;
		s = new Shape(imageShapes[imageIndex]);
		s->loadMesh();
		GLUTMainLoop();
	}
	
	void getImage() {
		if (imageIndex == -1) return;
		int imageSize = 500;
		string imagePath = s->meshPath.erase(s->meshPath.length() - 4) + ".bmp";
	
		// Make the BYTE array, factor of 3 because it's RBG.
		BYTE* pixels = new BYTE[3 * imageSize * imageSize];
	
		glReadPixels(100, 100, imageSize, imageSize, GL_RGB, GL_UNSIGNED_BYTE, pixels);
	
		// Convert to FreeImage format & save to file
		FIBITMAP* image = FreeImage_ConvertFromRawBits(pixels, imageSize, imageSize, 3 * imageSize, 24, 0x0000FF, 0xFF0000, 0x00FF00, false);
		FreeImage_Save(FIF_BMP, image, imagePath.c_str(), 0);
	
		// Free resources
		FreeImage_Unload(image);
		delete[] pixels;
		// update the index and load the new file
		imageIndex++;
		if (imageIndex > 380) return;
		delete s;
		s = new Shape(imageShapes[imageIndex]);
		s->loadMesh();
		GLUTMainLoop();
	}
#endif
#pragma endregion

#pragma region Results
// Create a second window showing the query results
void showResults() {
	if (shapes.empty()) return;
	// We only show the 12 best results
	Mat images[12];
	int i, m, n;
	for (int i = 0; i < 12; i++)
	{
		images[i] = imread(workingDirectory + shapes[i].meshPath.erase(shapes[i].meshPath.length() - 4) + ".bmp");
	}
	Mat DispImage = Mat::zeros(Size(100 + 150 * 4, 60 + 150 * 3), CV_8UC3);
	DispImage.setTo(cv::Scalar(255, 255, 255));
	for (i = 0, m = 20, n = 20; i < 12; i++, m += (20 + 150)) {
		// Get the Pointer to the IplImage
		Mat img = images[i];

		// Check whether it is NULL or not
		// If it is NULL, release the image, and return
		if (img.empty()) {
			std::cout << "failed to load shapes" << endl;
			return;
		}

		// Find whether height or width is greater in order to resize the image
		int max = (img.cols > img.rows) ? img.cols : img.rows;
		// Find the scaling factor to resize the image
		float imageScale = (float)((float)max / 150);
		// Used to Align the images
		if (i % 4 == 0 && m != 20) {
			m = 20;
			n += 20 + 150;
		}
		// Set the image ROI to display the current image
		// Resize the input image and copy the it to the Single Big Image
		Rect ROI(m, n, (int)(img.cols / imageScale), (int)(img.rows / imageScale));
		Mat temp; resize(img, temp, Size(ROI.width, ROI.height));
		float confidenceValue = 1.0f - (neighbor_dist[i] / 1);
		cv::putText(temp, std::to_string(confidenceValue), cv::Point(15, 140), cv::FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 0), 2, true);
		
		temp.copyTo(DispImage(ROI));		
	}
	cv::imshow("results", DispImage);
}
#pragma endregion

#pragma region Querying
void queryMesh() {
	OPENFILENAME ofn;
	::memset(&ofn, 0, sizeof(ofn));
	char f1[MAX_PATH];
	f1[0] = 0;
	ofn.lStructSize = sizeof(ofn);
	ofn.lpstrTitle = "Select A File";
	ofn.lpstrFilter = "OFF Files\0*.off\0All Files\0*.*\0\0";
	ofn.nFilterIndex = 2;
	ofn.lpstrFile = f1;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_FILEMUSTEXIST;

	// If a file has been selected we print the path to that file in the console
	if (::GetOpenFileName(&ofn) != FALSE)
	{
		std::cout << ofn.lpstrFile << std::endl;
	}

	// We load the actual mesh
	s = new Shape(ofn.lpstrFile, "NONE", "NONE");
	s->loadMesh();

	// We compute the feature vector of this mesh
	FeatureVector queryObject = s->getAllFeatures();
	queryObject.normalise(meanFeatureVector);



	// Set to true to use the ANNOY algorithm, false to use the brute force
#ifdef ANNOY
	float* vectorForQuery = queryObject.getAnnoyFeature();
	vector<int> neighbor_index;
	neighbor_dist;
	theIndex->get_nns_by_vector(vectorForQuery, 12, -1, &neighbor_index, &neighbor_dist);


	// Load the found shapes into the list of shapes
	shapes.clear();
	for (int i = 0; i < neighbor_index.size(); i++) {
		FeatureVector f = featureVectors[neighbor_index[i]];			//Get the feature vector that is at the index that is given by ANNOY
		shapes.push_back(Shape(f.meshPath, f.classification, "NONE"));	//Construct a shape out of that
	}
#else
	// We determine the distance from this feature vector to all the other feature vectors
	query = vector<tuple<float, FeatureVector>>();
	for (int i = 0; i < featureVectors.size(); i++) {
		FeatureVector& f = featureVectors[i];

		float dist = 0.0f;
		for (int elem = 0; elem < 5; elem++) {
			dist += abs(f.elemantaryDescriptors[elem] - queryObject.elemantaryDescriptors[elem]);
			dist += 1 * abs(f.distances[elem] - queryObject.distances[elem]);
		}
		query.push_back(make_tuple(dist, f));
	}

	sort(query.begin(), query.end(), [](const tuple<float, FeatureVector>& a, const tuple<float, FeatureVector>& b) -> bool
		{
			return (get<0>(a) < get<0>(b));
		});

	shapes.clear();
	for (int i = 0; i < query.size(); i++) {
		FeatureVector f = get<1>(query[i]);
		shapes.push_back(Shape(f.meshPath, f.classification, "NONE"));
	}
#endif

	// We diverted from the main loop, we have to find our way back
	s->loadMesh();
	GLUTMainLoop();
}

#pragma endregion

// Goes over all the files in both the datasets and makes an shape class for it
vector<Shape> getFilesInFolder() {
	// The path to the dataset folder
	std::string path = "../../Datasets";
	vector<Shape> shapes = vector<Shape>();

	// First get the paths for the PSB dataset
	for (const auto& subFolder : std::filesystem::directory_iterator(path + "/PSB")) {
		// There are folders within the path which we will have to open, we will iterate over those (these folders are the classifications)
		std::filesystem::path classificationFolder = subFolder.path();

		// We iterate over the files within the folder
		for (const auto& entry : std::filesystem::directory_iterator(classificationFolder.string())) {
			std::filesystem::path fileEntry = entry.path();

			// In these folders there are both .off and .txt files, we only want the off file (we add those to the path list)
			if (fileEntry.extension().string() == ".off") {
				
				// Split the line in parts (based on the '\\') to get the filename xxx.off, which will be used to save the feature vector
				std::stringstream ss(fileEntry.string());
				std::string token;
				vector<string> parts = vector<string>();
				while (std::getline(ss, token, '\\')) {
					parts.push_back(token);
				}
				shapes.push_back(Shape(fileEntry.string(), classificationFolder.filename().string(), parts[parts.size() - 1]));
			}
		}
	}

	return shapes;
	shapes.clear();

	// We need to make a list with all the classes for each filename
	std::map<string, string> classifications;
	std::string line;

	// Specify and open the file, first test
	ifstream myfile(path + "/Princeton/classification/v1/coarse1/coarse1Test.cla");
	if (myfile.is_open()) {
		// The first 5 lines can be ignored
		for (int i = 0; i < 2; i++) getline(myfile, line);

		// We get the first line that has actual data
		while (getline(myfile, line)) {
			// We ignore empty lines
			if (line == "") continue;

			// Split the line in parts (based on the ' ')
			std::stringstream ss(line);
			std::string token;
			vector<string> parts = vector<string>();
			while (std::getline(ss, token, ' ')) {
				parts.push_back(token);
			}

			// The last part is the number of elements, the first part is the classification for those elements
			int counter = stoi(parts[parts.size() - 1]);
			string classification = parts[0];

			for (int i = 0; i < counter; i++) {
				// Get the name/number/id of the file
				getline(myfile, line);
				classifications["m" + line + ".off"] = classification;
			}
		}
		myfile.close();
	}
	// Then train
	ifstream myfileTwo(path + "/Princeton/classification/v1/coarse1/coarse1Train.cla");
	if (myfileTwo.is_open()) {
		// The first 5 lines can be ignored
		for (int i = 0; i < 2; i++) getline(myfileTwo, line);

		// We get the first line that has actual data
		while (getline(myfileTwo, line)) {
			// We ignore empty lines
			if (line == "") continue;

			// Split the line in parts (based on the ' ')
			std::stringstream ss(line);
			std::string token;
			vector<string> parts = vector<string>();
			while (std::getline(ss, token, ' ')) {
				parts.push_back(token);
			}

			// The last part is the number of elements, the first part is the classification for those elements
			int counter = stoi(parts[parts.size() - 1]);
			string classification = parts[0];

			for (int i = 0; i < counter; i++) {
				// Get the name/number/id of the file
				getline(myfileTwo, line);
				classifications["m" + line + ".off"] = classification;
			}
		}
		myfileTwo.close();
	}

	// Then we get everything for the Princeton set, there are a lot of folders so we do this in the following way
	// 1. We initialize the vector with the root path
	// 2. While the vector is not empty we pop a path and scan that path
	// 2.1 If the element is a folder we add it to the list of paths to scan
	// 2.2 If the element is a file we check if it has the off extension, if it has we add it to the paths list
	vector<string> recursivePaths = vector<string>();
	recursivePaths.push_back(path + "/Princeton/db");
	while (recursivePaths.size() > 0) {
		string thePath = recursivePaths.back();
		recursivePaths.pop_back();

		for (const auto& entry : std::filesystem::directory_iterator(thePath)) {
			std::filesystem::path fileEntry = entry.path();

			// If its a directory we push it onto the stack
			if (std::filesystem::is_directory(entry)) {
				recursivePaths.push_back(fileEntry.string());
			}

			// If it is a file AND it has the off extenstion we add it to the list of files
			else {
				if (fileEntry.extension().string() == ".off") {
					// Split the line in parts (based on the '\\')
					std::stringstream ss(fileEntry.string());
					std::string token;
					vector<string> parts = vector<string>();
					while (std::getline(ss, token, '\\')) {
						parts.push_back(token);
					}
					string classification = classifications[parts[parts.size() - 1]];

					shapes.push_back(Shape(fileEntry.string(), classification, parts[parts.size() - 1]));
				}
			}
		}
	}
	return shapes;	
}

#pragma region Feature vector functions

void determineStandardDeviation(vector<Shape> shapes) {
	// Number of standard deviations that you have to be away before you are an outlier
	int stdDevNeeded = 1;

	// Create a vector for the vertex count
	std::vector<int> vertexCount = vector<int>();		// Vector with all individual vertex numbers
	int vertexSum = 0;									// The sum to compute the total number of vertices

	// Loop over all existing shapes
	for (int i = 0; i < shapes.size(); i++)
	{
		shapes[i].loadMesh();

		// Save the number of vertices for this mesh
		vertexCount.push_back(shapes[i].mesh->nverts);
		vertexSum += shapes[i].mesh->nverts;
	}

	// Compute the mean number of vertices
	int meanNumberVertices = vertexSum / vertexCount.size();
	int variance = 0;

	// Determine the variance for the set of vertices
	for (int i = 0; i < vertexCount.size(); i++) {
		int difference = vertexCount[i] - meanNumberVertices;
		variance += (difference * difference);
	}

	// We need to divide by the number of samples to determine the actual variance
	variance = variance / vertexCount.size();
	int stdDeviation = sqrt(variance);

	// We compute the minimum difference needed to be be 'stdDevNeeded' away from the mean (faster than using a division every time)
	int neededDiff = stdDevNeeded * stdDeviation;
}

void loadFeatureVectors() {
	// We iterate over the files within the folder
	for (const auto& entry : std::filesystem::directory_iterator("..\\..\\Datasets\\featureVectors\\")) {
		std::filesystem::path fileEntry = entry.path();

		// In these folders there are both .off and .txt files, we only want the off file (we add those to the path list)
		if (fileEntry.extension().string() == ".off") {

			FeatureVector f = FeatureVector();
			f.load(fileEntry.string());

			// We get the classification from the filename
			std::stringstream ss(f.meshPath);
			std::string token;
			vector<string> parts = vector<string>();
			while (std::getline(ss, token, '\\')) {
				parts.push_back(token);
			}
			f.classification = parts[parts.size() - 2];

			// Save this feature vector
			featureVectors.push_back(f);
		}
	}

	// Normalize all feature vectors	
	normalizeFeatureVectors();

	// Create index parameter
	theIndex = new MyAnnoyIndex(10);

#ifdef BUILD_ANNOY_INDEX 
	// We make the ANNOY index file
	std::vector<float> tmp(10);
	for (int i = 0; i < featureVectors.size(); i++) {
		float* tmp2 = featureVectors[i].getAnnoyFeature();				//We get the feature vector, but it is a float* and we need a vector<float>
		tmp.clear();													//We clear the existing vector
		for (int j = 0; j < 10; j++) { tmp.push_back(tmp2[j]); }		//And add al elements of the float* to it
		theIndex->add_item(i, tmp.data());
	}
	theIndex->build(50);
	theIndex->save("annoy.index");
#else
	// We read the ANNOY index file, set to true to load the index
	theIndex->load("annoy.index");
#endif
}

void normalizeFeatureVectors() {
	// We create a new mean feature vector
	meanFeatureVector = FeatureVector();

	// Compute the average for all parts of the feature vector (the loose elements + the histograms)
	vector<float> elementaryMean = vector<float>(); for (int i = 0; i < 5; i++) { elementaryMean.push_back(0.0f); }
	vector<float> elementaryVariance = vector<float>(); for (int i = 0; i < 5; i++) { elementaryVariance.push_back(0.0f); }
	vector<int> histograms = vector<int>(); for (int i = 0; i < 5 * bins; i++) { histograms.push_back(0); }

	for (int i = 0; i < featureVectors.size(); i++) {
		FeatureVector& f = featureVectors[i];

		// Summing the elementery parts
		for (int elem = 0; elem < 5; elem++) {
			elementaryMean[elem] += f.elemantaryDescriptors[elem];
		}

		// Summing the histogram parts
		for (int elem = 0; elem < bins; elem++) {
			histograms[0 * bins + elem] += f.angleThreeRandomVerts[elem];
			histograms[1 * bins + elem] += f.distanceBaryToRandomVert[elem];
			histograms[2 * bins + elem] += f.distanceTwoRandomVerts[elem];
			histograms[3 * bins + elem] += f.squareRootAreaTriangle[elem];
			histograms[4 * bins + elem] += f.cubeRootVolumeOfTetrahedron[elem];
		}
	}

	// Calculating mean of histogram parts
	for (int elem = 0; elem < bins; elem++) {
		histograms[0 * bins + elem] /= featureVectors.size();
		histograms[1 * bins + elem] /= featureVectors.size();
		histograms[2 * bins + elem] /= featureVectors.size();
		histograms[3 * bins + elem] /= featureVectors.size();
		histograms[4 * bins + elem] /= featureVectors.size();
	}

	// These are the five new "mean" histograms for each feature -> we save them to the mean historgram
	vector<int> h1 = vector<int>(); for (int i = 0; i < bins; i++) { h1.push_back(histograms[0 * bins + i]); } meanFeatureVector.angleThreeRandomVerts = h1;
	vector<int> h2 = vector<int>(); for (int i = 0; i < bins; i++) { h2.push_back(histograms[1 * bins + i]); } meanFeatureVector.distanceBaryToRandomVert = h2;
	vector<int> h3 = vector<int>(); for (int i = 0; i < bins; i++) { h3.push_back(histograms[2 * bins + i]); } meanFeatureVector.distanceTwoRandomVerts = h3;
	vector<int> h4 = vector<int>(); for (int i = 0; i < bins; i++) { h4.push_back(histograms[3 * bins + i]); } meanFeatureVector.squareRootAreaTriangle = h4;
	vector<int> h5 = vector<int>(); for (int i = 0; i < bins; i++) { h5.push_back(histograms[4 * bins + i]); } meanFeatureVector.cubeRootVolumeOfTetrahedron = h5;


	// Calculating mean of elementery parts
	for (int elem = 0; elem < 5; elem++) {
		elementaryMean[elem] /= featureVectors.size();
		meanFeatureVector.elemantaryDescriptors[elem] = elementaryMean[elem];
	}

	// Calculate the variance for the elementery parts
	for (int i = 0; i < featureVectors.size(); i++) {
		FeatureVector& f = featureVectors[i];

		for (int elem = 0; elem < 5; elem++) {
			elementaryVariance[elem] += pow(f.elemantaryDescriptors[elem] - elementaryMean[elem], 2);
		}
	}

	// Convert the variance to standard deviation
	meanFeatureVector.standDeviationElementaryDescriptors = new float[5];
	for (int elem = 0; elem < 5; elem++) {
		elementaryVariance[elem] /= featureVectors.size();													// Convert the variance to standard deviation (step 1/2)
		meanFeatureVector.standDeviationElementaryDescriptors[elem] = sqrtf(elementaryVariance[elem]);		// Convert the variance to standard devaition (step 2/2) + save it in the mean feature vector
	}

	// Now we can actually normalize everything
	for (int i = 0; i < featureVectors.size(); i++) {
		featureVectors[i].normalise(meanFeatureVector);
	}
}

#pragma endregion

void runEvaluation() {
	// First we seek al existing classifications
	std::map<string, int> values;
	for (int i = 0; i < featureVectors.size(); i++) {
		values[featureVectors[i].classification] = 0;
	}

	vector<string> classifications = vector<string>();
	for (auto const& imap : values) {
		classifications.push_back(imap.first);
	}

	// Initialize the whole thing
	std::map<string, std::map<string, int>> confusionMatrix;
	for (int i = 0; i < classifications.size(); i++) {
		for (int j = 0; j < classifications.size(); j++) {
			confusionMatrix[classifications[i]][classifications[j]] = 0;
		}
	}

	// Using ANNOY
	/*
	for (int i = 0; i < featureVectors.size(); i++) {
		float* vectorForQuery = featureVectors[i].getAnnoyFeature();
		vector<int> neighbor_index;
		neighbor_dist;
		theIndex->get_nns_by_vector(vectorForQuery, 12, -1, &neighbor_index, &neighbor_dist);

		// Load the found shapes into the list of shapes
		shapes.clear();
		for (int j= 0; j < neighbor_index.size(); j++) {
			FeatureVector f = featureVectors[neighbor_index[j]];			//Get the feature vector that is at the index that is given by ANNOY
			string foundClass = f.classification;
			cout << foundClass << std::endl;
			confusionMatrix[featureVectors[i].classification][foundClass]++;
		}
	}
	*/

	// Using brute force
	// Then we do a lookup for each feature vector and look at the best 12	
	for (int i = 0; i < featureVectors.size(); i++) {
		// We get the feature vector for this object
		FeatureVector& queryObject = featureVectors[i];

		// We determine the distance from this feature vector to all the other feature vectors
		query = vector<tuple<float, FeatureVector>>();
		for (int i = 0; i < featureVectors.size(); i++) {
			FeatureVector& f = featureVectors[i];

			float dist = 0.0f;
			for (int elem = 0; elem < 5; elem++) {
				dist += abs(f.elemantaryDescriptors[elem] - queryObject.elemantaryDescriptors[elem]);
				dist += 3 * abs(f.distances[elem] - queryObject.distances[elem]);
			}
			query.push_back(make_tuple(dist, f));
		}

		// We sort the list of hits from best to worst
		sort(query.begin(), query.end(), [](const tuple<float, FeatureVector>& a, const tuple<float, FeatureVector>& b) -> bool
			{
				return (get<0>(a) < get<0>(b));
			});

		// We get the first 12 elements and see how many are of the same classification
		for (int q = 0; q < 12; q++) {
			string foundClass = get<1>(query[q]).classification;
			confusionMatrix[queryObject.classification][foundClass]++;
		}
	}

	// We save the thing as a csv file
	std::ofstream file("confusionMatrix.csv");
	if (file.is_open()) {
		// We write the top line first
		string output = ",";
		for (int i = 0; i < classifications.size(); i++) {
			output += classifications[i] + ",";
		}
		file << output << "\n";

		// Now we write all the other lines
		for (int i = 0; i < classifications.size(); i++) {
			output = classifications[i] + ",";
			string index = classifications[i];
			for (int j = 0; j < classifications.size(); j++) {
				string index2 = classifications[j];
				int res = confusionMatrix[index][index2];
				output += std::to_string(res) + ",";
			}
			file << output << "\n";
		}
		file.close();
	}

	// We also make the True positive/false negative for each class
	std::ofstream file2("score.csv");
	file2 << "Class,Correct,Wrong" << "\n";
	if (file2.is_open()) {
		float prec = 0.0f;
		for (int i = 0; i < classifications.size(); i++) {
			string index = classifications[i];
			int correct = 0;
			int wrong = 0;
			for (int j = 0; j < classifications.size(); j++) {
				string index2 = classifications[j];
				if (index == index2) {
					correct += confusionMatrix[index][index2];
				}
				else {
					wrong += confusionMatrix[index][index2];
				}
			}
			prec += ((float)correct / ((float)correct + (float)wrong));
			file2 << index << "," + std::to_string(correct) + "," + std::to_string(wrong) + "\n";
		}
		file2 << "precision: " << (prec / classifications.size());
	}
	
}


#pragma region GLUT functions

void GLUTRedraw()
{
	// make glut not draw over glui
	glutSetWindow(GLUTwindow);
	glutPostRedisplay();
	// Setup viewing transformation
	glLoadIdentity();
	glScalef(scale, scale, scale);
	glTranslatef(translation[0], translation[1], 0.0);

	// Set projection transformation
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (GLfloat)GLUTwindow_width / (GLfloat)GLUTwindow_height, 0.1, 100.0);

	// Set camera transformation
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translation[0], translation[1], translation[2]);
	glScalef(scale, scale, scale);
	glRotatef(rotation[0], 1.0, 0.0, 0.0);
	glRotatef(rotation[1], 0.0, 1.0, 0.0);
	glRotatef(rotation[2], 0.0, 0.0, 1.0);
	glTranslatef(-center[0], -center[1], -center[2]);

	// Clear window 
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set lights
	static GLfloat light0_position[] = { 3.0, 4.0, 5.0, 0.0 };
	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	static GLfloat light1_position[] = { -3.0, -2.0, -3.0, 0.0 };
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);

	// Set material
	static GLfloat material[] = { 1.0, 0.5, 0.5, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material);

	// drawing mode
	byte drawingMode = GL_POLYGON;
	if (wireframe) drawingMode = GL_LINES;
	if (s != NULL)
	{
		// Draw faces
		for (int i = 0; i < s->mesh->nfaces; i++) {
			Face& face = s->mesh->faces[i];
			glBegin(drawingMode);
			glNormal3fv(face.normal);
			for (int j = 0; j < face.nverts; j++) {
				Vertex vert = face.verts[j];
				glVertex3f(vert.x, vert.y, vert.z);
			}
			glEnd();
		}
	}
	// Swap buffers 
	glutSwapBuffers();
}

void GLUTStop(void)
{
	// Destroy window 
	glutDestroyWindow(GLUTwindow);

	// Exit
	exit(0);
}

void GLUTResize(int w, int h)
{
	// Resize window
	glViewport(0, 0, w, h);

	// Remember window size 
	GLUTwindow_width = w;
	GLUTwindow_height = h;
	// Redraw
	glutPostRedisplay();
}

void GLUTMotion(int x, int y)
{
	// Invert y coordinate
	y = GLUTwindow_height - y;

	// Process mouse motion event
	if (rotating) {
		// Rotate model
		rotation[0] += -0.5 * (y - GLUTmouse[1]);
		rotation[2] += 0.5 * (x - GLUTmouse[0]);
	}
	else if (scaling) {
		// Scale window
		scale *= exp(2.0 * (float)(x - GLUTmouse[0]) / (float)GLUTwindow_width);
	}
	else if (translating) {
		// Translate window
		translation[0] += 2.0 * (float)(x - GLUTmouse[0]) / (float)GLUTwindow_width;
		translation[1] += 2.0 * (float)(y - GLUTmouse[1]) / (float)GLUTwindow_height;
	}

	// Remember mouse position 
	GLUTmouse[0] = x;
	GLUTmouse[1] = y;
}

void GLUTMouse(int button, int state, int x, int y)
{
	// Invert y coordinate
	y = GLUTwindow_height - y;

	// Process mouse button event
	rotating = (button == GLUT_LEFT_BUTTON);
	scaling = (button == GLUT_MIDDLE_BUTTON);
	translating = (button == GLUT_RIGHT_BUTTON);
	if (rotating || scaling || translating) glutIdleFunc(GLUTRedraw);
	else glutIdleFunc(0);

	// Remember button state 
	int b = (button == GLUT_LEFT_BUTTON) ? 0 : ((button == GLUT_MIDDLE_BUTTON) ? 1 : 2);
	GLUTbutton[b] = (state == GLUT_DOWN) ? 1 : 0;

	// Remember modifiers 
	GLUTmodifiers = glutGetModifiers();

	// Remember mouse position 
	GLUTmouse[0] = x;
	GLUTmouse[1] = y;
}

void GLUTSpecial(int key, int x, int y)
{
	// Invert y coordinate
	y = GLUTwindow_height - y;

	// Remember mouse position 
	GLUTmouse[0] = x;
	GLUTmouse[1] = y;

	// Remember modifiers 
	GLUTmodifiers = glutGetModifiers();

	// Redraw
	glutPostRedisplay();
}

void GLUTKeyboard(unsigned char key, int x, int y)
{
	// Process keyboard button event 
	switch (key) {
	case 'Q':
	case 'q':
		GLUTStop();
		break;

	case 27: // ESCAPE
		GLUTStop();
		break;
	}

	// Remember mouse position 
	GLUTmouse[0] = x;
	GLUTmouse[1] = GLUTwindow_height - y;

	// Remember modifiers 
	GLUTmodifiers = glutGetModifiers();
}

void GLUTInit(int* argc, char** argv)
{
	// Open window 
	glutInit(argc, argv);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(GLUTwindow_width, GLUTwindow_height);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); // | GLUT_STENCIL
	GLUTwindow = glutCreateWindow("OpenGL Viewer");

	// Initialize GLUT callback functions 
	glutReshapeFunc(GLUTResize);
	glutDisplayFunc(GLUTRedraw);
	glutKeyboardFunc(GLUTKeyboard);
	glutSpecialFunc(GLUTSpecial);
	glutMouseFunc(GLUTMouse);
	glutMotionFunc(GLUTMotion);
	glutIdleFunc(0);

	// Initialize lights 
	static GLfloat lmodel_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	static GLfloat light0_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
	glEnable(GL_LIGHT0);
	static GLfloat light1_diffuse[] = { 0.5, 0.5, 0.5, 1.0 };
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
	glEnable(GL_LIGHT1);
	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHTING);

	// Initialize graphics modes 
	glEnable(GL_DEPTH_TEST);
}

void GLUTMainLoop(void)
{
	// Compute bounding box
	float bbox[2][3] = { { 1.0E30F, 1.0E30F, 1.0E30F }, { -1.0E30F, -1.0E30F, -1.0E30F } };
	if (s != NULL)
	{
		for (int i = 0; i < s->mesh->nverts; i++) {
			Vertex& vert = s->mesh->verts[i];
			if (vert.x < bbox[0][0]) bbox[0][0] = vert.x;
			else if (vert.x > bbox[1][0]) bbox[1][0] = vert.x;
			if (vert.y < bbox[0][1]) bbox[0][1] = vert.y;
			else if (vert.y > bbox[1][1]) bbox[1][1] = vert.y;
			if (vert.z < bbox[0][2]) bbox[0][2] = vert.z;
			else if (vert.z > bbox[1][2]) bbox[1][2] = vert.z;
		}
	}

	// Setup initial viewing scale
	float dx = bbox[1][0] - bbox[0][0];
	float dy = bbox[1][1] - bbox[0][1];
	float dz = bbox[1][2] - bbox[0][2];
	scale = 2.0 / sqrt(dx * dx + dy * dy + dz * dz);

	// Setup initial viewing center
	center[0] = 0.5 * (bbox[1][0] + bbox[0][0]);
	center[1] = 0.5 * (bbox[1][1] + bbox[0][1]);
	center[2] = 0.5 * (bbox[1][2] + bbox[0][2]);

	// Run main loop -- never returns 
	glutMainLoop();
}

#pragma endregion